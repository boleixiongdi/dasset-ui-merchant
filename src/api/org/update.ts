
import bsinRequest from "@/utils/bsinrequest";

// 修改机构信息
export async function updateOrgInfo(params: object) {
    console.log('updateOrgInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OrgSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

