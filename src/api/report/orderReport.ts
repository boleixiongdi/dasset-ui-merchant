
import bsinRequest from "@/utils/bsinrequest";

// 订单量报表
export async function getOrderDate(params: object) {
    console.log('getOrderDate');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "getOrderTrends",
            },
            body: {
                ...params,
            }
        }
    })
}

