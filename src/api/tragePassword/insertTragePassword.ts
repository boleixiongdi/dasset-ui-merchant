
import bsinRequest from "@/utils/bsinrequest";

// 新增交易密码
export async function insertTragePassword(params: object) {
    console.log('insertTragePassword');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "TxPwdMngSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

