
import bsinRequest from "@/utils/bsinrequest";

// 重置交易密码
export async function getAccount(params: object) {
    console.log('getAccount');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "getAcctNo",
            },
            body: {
                ...params,
            }
        }
    })
}

