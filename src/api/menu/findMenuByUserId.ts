import bsinRequest from "@/utils/bsinrequest";

// 根据用户号查询改用户拥有权限的对应菜单
export async function findMenuByUserId(params: object) {
    console.log('findMenuByUserId');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "findMenuByUserId",      
            },
            body: {
                ...params,
            }
        }
    })
}

