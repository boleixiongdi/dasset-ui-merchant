
import bsinRequest from "@/utils/bsinrequest";

// 提现查询
export async function drawcash(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "WithdrawApplySVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

