
import bsinRequest from "@/utils/bsinrequest";

// 当前充值提现汇率查询
export async function getExchangeRate(params: object) {
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "VirtualAccountSVC",
                stdIntfcInd: "calculateFee",
            },
            body: {
                ...params,
            }
        }
    })
}

