
import bsinRequest from "@/utils/bsinrequest";

// 重置商户密码
export async function updatePassword(params: object) {
    console.log('updatePassword');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerPwdMngSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

