import { h } from 'vue';
import { NAvatar } from 'naive-ui';

export const columns = [
  
  {
    title: '序号',
    key: 'number',
    width: 100,
  },
 
  {
    title: '商品订单号',
    key: 'prodOrdNo',
    width: 100,
  },
  {
    title: '会员号',
    key: 'memNo',
    width: 100,
  },
  {
    title: '退款申请日期',
    key: 'applyDate',
    width: 100,
  },
  {
    title: '退款申请金额',
    key: 'refundAmt',
    width: 100,
  },
 
  {
    title: '币种',
    key: 'ccy',
    width: 100,
  },
  {
    title: '退款处理状态',
    key: 'status',
    width: 100,
  },
  {
    title: '退款失败原因',
    key: 'refuseReason',
    width: 100,
  },
  {
    title: '退款申请流水号',
    key: 'applicationNo',
    width: 100,
  },
];
