
import bsinRequest from "@/utils/bsinrequest";

// 查看菜单
export async function viewMenuInfo(params: object) {
    console.log('viewMenuInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "view",
            },
            body: {
                ...params,
            }
        }
    })
}

