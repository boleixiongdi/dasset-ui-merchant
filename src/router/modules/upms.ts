import { RouteRecordRaw } from 'vue-router';
import { Layout } from '@/router/constant';
import { KeyOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';

/**
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由附带扩展信息）
 * @param redirect 重定向地址, 访问这个路由时,自定进行重定向
 * @param meta.disabled 禁用整个菜单
 * @param meta.title 菜单名称
 * @param meta.icon 菜单图标
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前
 *
 * */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/upms',
    name: 'upms',
    redirect: '/upms/upms',
    component: Layout,
    meta: {
      title: '权限管理',
      icon: renderIcon(KeyOutlined),
      sort: 5,
    },
    children: [
      {
        path: 'user',
        name: 'user',
        meta: {
          title: '用户管理',
        },
        component: () => import('@/views/upms/user/user.vue'),
      },
      {
        path: 'role',
        name: 'role',
        meta: {
          title: '角色管理',
        },
        component: () => import('@/views/upms/role/role.vue'),
      },
      {
        path: 'org',
        name: 'org',
        meta: {
          title: '机构管理',
        },
        component: () => import('@/views/upms/org/org.vue'),
      },
      {
        path: 'menu',
        name: 'menu',
        meta: {
          title: '菜单管理',
        },
        component: () => import('@/views/upms/menu/menu.vue')
      },
      {
        path: 'operation',
        name: 'operation',
        meta: {
          title: '操作管理',
        },
        component: () => import('@/views/upms/operation/operation.vue')
      },
    ],
  },
];

export default routes;
