
export const columns = [
  {
    title: '操作ID',
    key: 'operatorId',
    width: 120,
  },
  {
    title: '操作名称',
    key: 'name',
    width: 120,
  },
  {
    title: '操作英文名',
    key: 'nameEn',
    width: 120,
  },
  {
    title: '操作代码',
    key: 'code',
    width: 120,
  },
  {
    title: '标准服务标识',
    key: 'stdSvcInd',
    width: 120,
  },
  {
    title: '标准接口方法',
    key: 'stdIntfInd',
    width: 100,
  },
];
