export const columns = [
  {
    title: '用户ID',
    key: 'userId',
    width: 80,
  },
  {
    title: '登录名',
    key: 'loginName',
    width: 100,
  },
  {
    title: '登录全名',
    key: 'fullName',
    width: 80,
  },
  {
    title: '手机号码',
    key: 'tel',
    width: 80,
  },
  {
    title: '用户状态',
    key: 'status',
    width: 80,
  },
  {
    title: '创建时间',
    key: 'createdTime',
    width: 100,
  },
  {
    title: '邮箱',
    key: 'email',
    width: 100,
  },
  
];
