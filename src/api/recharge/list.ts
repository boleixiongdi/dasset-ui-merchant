
import bsinRequest from "@/utils/bsinrequest";

// 充值
export async function rechargeList(params: object) {
    console.log('getUserList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "recharge",
            },
            body: {
                ...params,
            }
        }
    })
}

