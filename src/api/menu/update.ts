
import bsinRequest from "@/utils/bsinrequest";

// 修改菜单
export async function updateMenuInfo(params: object) {
    console.log('updateMenuInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

