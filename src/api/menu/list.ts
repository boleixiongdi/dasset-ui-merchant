
import bsinRequest from "@/utils/bsinrequest";

// 获取菜单列表
export async function getMenuList(params: object) {
    console.log('getMenuList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

