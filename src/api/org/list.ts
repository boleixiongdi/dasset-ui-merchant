
import bsinRequest from "@/utils/bsinrequest";

// 获取机构列表
export async function getOrgList(params: object) {
    console.log('getOrgList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OrgSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

