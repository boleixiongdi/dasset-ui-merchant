const getMenu = (e) => {
  let busB = {}
  let busC = []
  let busD = []
  e.map((val) => {
    busB.name = val.path
    busB.path = '/' + val.path
    busB.title = val.name
    busB.children = []
    if (val.children) {
      val.children.map((value) => {
        busC.name = value.path
        busC.path = '/' + value.path
        busC.title = value.name
        busB.children.push(busC)
        busC = {}
      })
    } else {
      delete busB.children
    }
    busD.push(busB)
    busB = {}
  })
  return busD
}
export default getMenu