
import bsinRequest from "@/utils/bsinrequest";

// 交易查询
export async function tradeInfoApi(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "queryTxnInfo",
            },
            body: {
                ...params,
            }
        }
    })
}

