import { h } from 'vue';
import { NAvatar } from 'naive-ui';

export const columns = [
  {
    title: '申请时间',
    key: 'requestTime',
    width: 80,
  },
  {
    title: '提现金额',
    key: 'withdrawAmount',
    width: 80,
  },
  {
    title: '提现币种',
    key: 'withdrawCcy',
    width: 80,
  },
  {
    title: '提现方式',
    key: 'withdrawMethod',
    width: 80,
  },
  {
    title: '目标账号',
    key: 'targetAccount',
    width: 80,
  },
  {
    title: '提现汇率',
    key: 'exgRate',
    width: 80,
  },
  {
    title: '货币对',
    key: 'ccyPair',
    width: 80,
  },
  {
    title: '提现手续费',
    key: 'fee',
    width: 80,
  },
  {
    title: '手续费币种',
    key: 'feeCcy',
    width: 80,
  },
  {
    title: '到账金额',
    key: 'actualAmount',
    width: 80,
  },
  {
    title: '到账金额币种',
    key: 'actualCcy',
    width: 80,
  },
  {
    title: '交易流水',
    key: 'txNo',
    width: 80,
  },
  {
    title: '交易HASH',
    key: 'txnHash',
    width: 80,
  },
  {
    title: '交易状态',
    key: 'txStatus',
    width: 80,
  },
  {
    title: '失败原因',
    key: 'reson',
    width: 80,
  },
];
