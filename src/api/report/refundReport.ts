
import bsinRequest from "@/utils/bsinrequest";

// 退款统计报表
export async function getRefundDate(params: object) {
    console.log('getSalesDate');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "ReportSVC",
                stdIntfcInd: "qryRefundStats",
            },
            body: {
                ...params,
            }
        }
    })
}

