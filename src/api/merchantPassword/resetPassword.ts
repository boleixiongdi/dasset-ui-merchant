
import bsinRequest from "@/utils/bsinrequest";

// 重置商户密码
export async function resetPassword(params: object) {
    console.log('resetPassword');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerPwdMngSVC",
                stdIntfcInd: "reset",
            },
            body: {
                ...params,
            }
        }
    })
}

