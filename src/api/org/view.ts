

import bsinRequest from "@/utils/bsinrequest";

// 查看机构信息
export async function viewOrgInfo(params: object) {
    console.log('viewOrgInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OrgSVC",
                stdIntfcInd: "view",
            },
            body: {
                ...params,
            }
        }
    })
}

