
import bsinRequest from "@/utils/bsinrequest";

// 获取商户信息
export async function getMerchantInfo(params: object) {
    console.log('getMerchantInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "getMchInfo",
            },
            body: {
                ...params,
            }
        }
    })
}

