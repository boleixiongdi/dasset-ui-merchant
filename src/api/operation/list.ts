
import bsinRequest from "@/utils/bsinrequest";

// 获取操作列表
export async function getOperationList(params: object) {
    console.log('getOperationList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OperatorSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

