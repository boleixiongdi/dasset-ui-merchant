
import bsinRequest from "@/utils/bsinrequest";

// 销售统计报表
export async function getSalesDate(params: object) {
    console.log('getSalesDate');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "ReportSVC",
                stdIntfcInd: "qrySalesStats",
            },
            body: {
                ...params,
            }
        }
    })
}

