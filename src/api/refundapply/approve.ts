
import bsinRequest from "@/utils/bsinrequest";

// 退款审批
export async function refundApprove(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RefundSVC",
                stdIntfcInd: "approve",
            },
            body: {
                ...params,
            }
        }
    })
}

