
import bsinRequest from "@/utils/bsinrequest";

// 根据客户号获取商户资产账号
export async function getAccount(params: object) {
    console.log('getAccount');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "getAcctNo",
            },
            body: {
                ...params,
            }
        }
    })
}

