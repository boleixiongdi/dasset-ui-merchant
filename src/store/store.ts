import { createStore } from 'vuex'
import storage from './storage' 
export default createStore({
  modules: { 
    storage: storage
  }
})