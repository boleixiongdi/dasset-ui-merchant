
import bsinRequest from "@/utils/bsinrequest";

// 历史汇率报表
export async function getExchangeDate(params: object) {
    console.log('getExchangeDate');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "getExcRateHistory",
            },
            body: {
                ...params,
            }
        }
    })
}

