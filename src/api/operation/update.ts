
import bsinRequest from "@/utils/bsinrequest";

// 修改操作
export async function updateOperationInfo(params: object) {
    console.log('updateOperationInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OperatorSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

