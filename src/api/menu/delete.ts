
import bsinRequest from "@/utils/bsinrequest";

// 删除菜单
export async function deleteMenuInfo(params: object) {
    console.log('deleteMenuInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "delete",        
            },
            body: {
                ...params,
            }
        }
    })
}

