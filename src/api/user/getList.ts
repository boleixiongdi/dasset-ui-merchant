
import bsinRequest from "@/utils/bsinrequest";

// 查询用户对应角色列表
export async function getList(params: object) {
    console.log('addUserInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserRoleSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

