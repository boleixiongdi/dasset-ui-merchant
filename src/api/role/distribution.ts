
import bsinRequest from "@/utils/bsinrequest";

// 查看角色信息
export async function distributionRole(params: object) {
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleOperatorSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

