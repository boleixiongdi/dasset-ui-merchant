import http from '@/utils/http/axios';
import bsinRequest from "@/utils/bsinrequest";

// 获取主控台信息
export async function getConsoleInfo(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "stats",
            },
            body: {
                ...params,
            }
        }
    })
}
// 获取今日汇率
export async function getExcRate(params: object) {
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "ExcRateMngSVC",
                stdIntfcInd: "excRate",
            },
            body: {
                ...params,
            }
        }
    })
}


