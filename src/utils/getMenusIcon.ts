
import { KeyOutlined } from '@vicons/antd';
import { renderIcon } from '@/utils/index';
import { TransactionOutlined } from '@vicons/antd';
import { DashboardOutlined } from '@vicons/antd';
import { DollarCircleOutlined } from '@vicons/antd';
import { MoneyCollectOutlined } from '@vicons/antd';
import { DisconnectOutlined } from '@vicons/antd';
import { SettingOutlined } from '@vicons/antd';
import { BarChartOutlined } from '@vicons/antd';

const getMenusIcon = (e) => {
  var busB = {}
  var busC = []
  var busD = []
  e.map((val) => {
    busB.name = val.path
    busB.path = '/' + val.path
    busB.title = val.name
    if (val.name === '主控台') {
      busB.icon = renderIcon(DashboardOutlined) // 主控台
    } else if (val.name === '提现查询') {
      busB.icon = renderIcon(DollarCircleOutlined) // 提现查询
    } else if (val.name === '充值查询') {
      busB.icon = renderIcon(MoneyCollectOutlined) // 充值查询
    } else if (val.name === '退款查询') {
      busB.icon = renderIcon(DisconnectOutlined) // 退款查询
    } else if (val.name === '交易管理') {
      busB.icon = renderIcon(TransactionOutlined) // 交易管理
    } else if (val.name === '权限管理') {
      busB.icon = renderIcon(KeyOutlined) // 权限管理
    } else if (val.name === '商户设置') {
      busB.icon = renderIcon(SettingOutlined) // 设置页面
    } else if (val.name === '数据报表') {
      busB.icon = renderIcon(BarChartOutlined) // 数据报表
    } else {
      busB.icon = renderIcon(DashboardOutlined) // 未传值
    }
    busB.children = []
    if (val.children[0] == undefined) {
      delete busB.children
    } else {
      val.children.map((value) => {
        busC.name = value.path
        busC.path = '/' + value.path
        busC.title = value.name
        busB.children.push(busC)
        busC = {}
      })
    }
    busD.push(busB)
    busB = {}
  })
  return busD
}
export default getMenusIcon