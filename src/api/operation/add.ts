
import bsinRequest from "@/utils/bsinrequest";

// 添加操作
export async function addOperationInfo(params: object) {
    console.log('addOperationInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OperatorSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

