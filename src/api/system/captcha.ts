
import bsinRequest from "@/utils/bsinrequest";

// 获取验证码
export async function getCaptcha(params: object) {
    console.log('getCaptcha');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserSVC",
                stdIntfcInd: "getValidatePicture",
            },
            body: {
                ...params,
            }
        }
    })
}

