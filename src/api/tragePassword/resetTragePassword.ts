
import bsinRequest from "@/utils/bsinrequest";

// 重置交易密码
export async function resetTragePassword(params: object) {
    console.log('resetTragePassword');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "TxPwdMngSVC",
                stdIntfcInd: "reset",
            },
            body: {
                ...params,
            }
        }
    })
}

