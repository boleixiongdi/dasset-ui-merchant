import { h } from 'vue';
import { NAvatar } from 'naive-ui';

export const columns = [
  {
    title: '充值时间',
    key: 'createdTime',
    width: 80,
  },
  {
    title: '充值方式',
    key: 'rechargeMode',
    width: 80,
  },
  {
    title: '充值金额',
    key: 'amt',
    width: 100,
  },
  {
    title: '充值金额币种',
    key: 'amtCcy',
    width: 100,
  },
  {
    title: '手续费',
    key: 'fee',
    width: 70,
  },
  {
    title: '手续费币种',
    key: 'feeCcy',
    width: 100,
  },
  {
    title: '付款账号',
    key: 'payNo',
    width: 80,
  },
  {
    title: '汇率',
    key: 'exgRate',
    width: 80,
  },
  {
    title: '货币对',
    key: 'ccyPair',
    width: 80,
  },
  {
    title: '到账金额',
    key: 'recAmount',
    width: 80,
  },
  {
    title: '到账金额币种',
    key: 'recCcy',
    width: 100,
  },
  {
    title: '状态',
    key: 'status',
    width: 70,
  },
  {
    title: '充值流水号',
    key: 'rcNo',
    width: 80,
  },
  {
    title: '交易HASH',
    key: 'txnHash',
    width: 80,
  },
  {
    title: '失败原因',
    key: 'remarks',
    width: 80,
  },
  // 
 
];
