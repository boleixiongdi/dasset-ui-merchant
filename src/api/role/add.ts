
import bsinRequest from "@/utils/bsinrequest";

// 添加用户信息
export async function addRoleInfo(params: object) {
    console.log('getUserList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

