
import bsinRequest from "@/utils/bsinrequest";

// 充值
export async function recharge(params: object) {
    console.log('充值');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "ChainWalletSVC",
                stdIntfcInd: "rechargeApply",
                // stdIntfcVerNo: "1.0.0",
                // consmSysInd: "NGMFEB",
                // txnChrctrstcType: "0",
                // srcConsmSysInd: "NGMFEB"
            },
            body: {
                ...params,
            }
        }
    })
}

