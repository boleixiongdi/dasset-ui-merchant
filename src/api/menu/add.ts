
import bsinRequest from "@/utils/bsinrequest";

// 添加菜单
export async function addMneuInfo(params: object) {
    console.log('addMneuInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MenuSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

