
export const columns = [
  {
    title: '角色ID',
    key: 'roleId',
    width: 120,
  },
  {
    title: '角色名称',
    key: 'name',
    width: 120,
  },
  {
    title: '角色代码',
    key: 'code',
    width: 120,
  },
  {
    title: '角色状态',
    key: 'status',
    width: 120,
  },
  {
    title: '创建时间',
    key: 'createdTime',
    width: 120,
  },
  {
    title: '角色描述',
    key: 'desc',
    width: 100,
  },
];
