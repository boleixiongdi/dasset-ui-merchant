import { h } from 'vue';
import { NAvatar } from 'naive-ui';

export const columns = [
  {
    title: '退款流水号',
    key: 'refundOrdNo',
    width: 80,
  },
  {
    title: '订单号',
    key: 'prodOrdNo',
    width: 80,
  },
  {
    title: '会员号',
    key: 'memNo',
    width: 80,
  },
  {
    title: '订单金额',
    key: 'txAmt',
    width: 80,
  },
  {
    title: '订单金额币种',
    key: 'txAmtCcy',
    width: 80,
  },
  {
    title: '退款申请日期',
    key: 'refundDte',
    width: 80,
  },
  {
    title: '退款申请金额',
    key: 'refundAmt',
    width: 80,
  },
  {
    title: '退款金额币种',
    key: 'ccy',
    width: 80,
  },
  {
    title: '退款状态',
    key: 'status',
    width: 80,
  },
  {
    title: '折扣退还金额',
    key: 'totalDisc',
    width: 80,
  },
  {
    title: '折扣币种',
    key: 'ccy',
    width: 80,
  },
  // {
  //   title: '会员折扣退还金额',
  //   key: 'memRefundDisc',
  //   width: 80,
  // },
  // {
  //   title: '会员折扣币种',
  //   key: 'ccy',
  //   width: 80,
  // },
];
