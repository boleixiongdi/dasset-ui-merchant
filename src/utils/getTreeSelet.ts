const getTreeSelet = (e) => {
  var busB = {}
  var busC = []
  var busD = []
  e.map((val) => {
    busB.label = val.name
    busB.key = val.menuId
    busB.value = val.menuId
    busB.children = []
    if (val.children) {
      val.children.map((value) => {
        busC.label = value.name
        busC.key = value.menuId
        busC.value = value.menuId
        busB.children.push(busC)
        busC = {}
      })
    } else {
      delete busB.children
    }
    busD.push(busB)
    busB = {}
  })
  return busD
}
export default getTreeSelet
// console.log(getTreeSelet(a)[0]);