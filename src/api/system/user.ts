import http from '@/utils/http/axios';
import bsinRequest from "@/utils/bsinrequest";

export interface BasicResponseModel<T = any> {
  code: number;
  message: string;
  result: T;
}

export interface BasicPageParams {
  pageNumber: number;
  pageSize: number;
  total: number;
}

/**
 * @description: 获取用户信息生成菜单
 */
export function getUserInfo(params) {
  console.log('getUserInfo');
  
  return bsinRequest('/agrs', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: "MenuSVC",
        stdIntfcInd: "findMenuByUserId",
      },
      body: {
        ...params,
      }
    }
  })
}

/**
 * @description: 用户登录
 */
export function login(params) {
  return bsinRequest('/agrs', {
    method: 'POST',
    data: {
      sysHead: {
        stdSvcInd: "UserSVC",
        stdIntfcInd: "login",

      },
      body: {
        ...params,
      }
    }
  })
}

/**
 * @description: 用户修改密码
 */
export function changePassword(params, uid) {
  return http.request(
    {
      url: `/user/u${uid}/changepw`,
      method: 'POST',
      params,
    },
    {
      isTransformResponse: false,
    }
  );
}

/**
 * @description: 用户登出
 */
export function logout(params) {
  return http.request({
    url: '/login/logout',
    method: 'POST',
    params,
  });
}
