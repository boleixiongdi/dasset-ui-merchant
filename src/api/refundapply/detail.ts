
import bsinRequest from "@/utils/bsinrequest";

// 退款详情
export async function refundDetail(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RefundSVC",
                stdIntfcInd: "detail",
            },
            body: {
                ...params,
            }
        }
    })
}

