
import bsinRequest from "@/utils/bsinrequest";

// 获取今日汇率
export async function tradeInfoApi(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "ExcRateMngSVC",
                stdIntfcInd: "excRate",
            },
            body: {
                ...params,
            }
        }
    })
}

