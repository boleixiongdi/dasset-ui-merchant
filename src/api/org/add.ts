
import bsinRequest from "@/utils/bsinrequest";

// 添加机构
export async function addOrgInfo(params: object) {
    console.log('addOrgInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OrgSVC",
                stdIntfcInd: "add",
            },
            body: {
                ...params,
            }
        }
    })
}

