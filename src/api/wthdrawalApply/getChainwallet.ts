
import bsinRequest from "@/utils/bsinrequest";

// 获取连钱包账号
export async function getChainwallet(params: object) {
    console.log('getChainwallet');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "WithdrawApplySVC",
                stdIntfcInd: "getChainWallet",
            },
            body: {
                ...params,
            }
        }
    })
}

