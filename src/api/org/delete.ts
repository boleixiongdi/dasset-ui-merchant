
import bsinRequest from "@/utils/bsinrequest";

// 删除机构信息
export async function deleteOrgInfo(params: object) {
    console.log('deleteOrgInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OrgSVC",
                stdIntfcInd: "delete",
            },
            body: {
                ...params,
            }
        }
    })
}

