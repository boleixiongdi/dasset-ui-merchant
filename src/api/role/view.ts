
import bsinRequest from "@/utils/bsinrequest";

// 查看角色信息
export async function viewRoleInfo(params: object) {
    console.log('viewRoleInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleSVC",
                stdIntfcInd: "view",
            },
            body: {
                ...params,
            }
        }
    })
}

