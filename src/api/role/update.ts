
import bsinRequest from "@/utils/bsinrequest";

// 修改角色信息
export async function updateRoleInfo(params: object) {
    console.log('getUserList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

