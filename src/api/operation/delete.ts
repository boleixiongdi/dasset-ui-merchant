
import bsinRequest from "@/utils/bsinrequest";

// 删除操作
export async function deleteOperationInfo(params: object) {
    console.log('deleteOperationInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OperatorSVC",
                stdIntfcInd: "delete",
            },
            body: {
                ...params,
            }
        }
    })
}

