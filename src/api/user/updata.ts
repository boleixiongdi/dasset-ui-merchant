
import bsinRequest from "@/utils/bsinrequest";

// 获取用户信息
export async function updataUserInfo(params: object) {
    console.log('updataUserInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}

