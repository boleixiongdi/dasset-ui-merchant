
import bsinRequest from "@/utils/bsinrequest";

// 获取用户信息
export async function addUserInfo(params: object) {
    console.log('addUserInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserSVC",
                stdIntfcInd: "addUser",
            },
            body: {
                ...params,
            }
        }
    })
}

