
import bsinRequest from "@/utils/bsinrequest";

// 获取用户信息
export async function getRoleList(params: object) {
    console.log('getUserList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

