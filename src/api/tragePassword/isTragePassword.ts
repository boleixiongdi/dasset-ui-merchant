
import bsinRequest from "@/utils/bsinrequest";

// 修改交易密码
export async function isTragePassword(params: object) {
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "TxPwdMngSVC",
                stdIntfcInd: "isExist",
            },
            body: {
                ...params,
            }
        }
    })
}
