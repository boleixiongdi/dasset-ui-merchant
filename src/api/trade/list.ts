
import bsinRequest from "@/utils/bsinrequest";

// 交易查询
export async function tradeListApi(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "queryTxn",
            },
            body: {
                ...params,
            }
        }
    })
}

