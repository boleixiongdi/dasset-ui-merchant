
import bsinRequest from "@/utils/bsinrequest";

// 获取用户信息
export async function getUserList(params: object) {
    console.log('getUserList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

