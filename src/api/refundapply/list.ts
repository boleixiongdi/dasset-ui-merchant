
import bsinRequest from "@/utils/bsinrequest";

// 退款查询
export async function refundapplyList(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "MerQuerySVC",
                stdIntfcInd: "refund",
            },
            body: {
                ...params,
            }
        }
    })
}

