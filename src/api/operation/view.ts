
import bsinRequest from "@/utils/bsinrequest";

// 查看操作信息
export async function viewOperatorInfo(params: object) {
    console.log('viewOperatorInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "OperatorSVC",
                stdIntfcInd: "view",
            },
            body: {
                ...params,
            }
        }
    })
}

