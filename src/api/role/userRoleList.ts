import bsinRequest from "@/utils/bsinrequest";

// 根据user查询role
export async function getUserRoleList(params: object) {
    console.log('getUserRoleList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserRoleSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

