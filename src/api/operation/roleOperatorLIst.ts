import bsinRequest from "@/utils/bsinrequest";

// 根据role查询operation
export async function getRoleOperationList(params: object) {
    console.log('getRoleOperationList');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleOperatorSVC",
                stdIntfcInd: "list",
            },
            body: {
                ...params,
            }
        }
    })
}

