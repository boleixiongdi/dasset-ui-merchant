
import bsinRequest from "@/utils/bsinrequest";

// 提现
export async function drawMoney(params: object) {
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "WithdrawApplySVC",
                stdIntfcInd: "apply",
            },
            body: {
                ...params,
            }
        }
    })
}

