import { h } from 'vue';
import { NAvatar } from 'naive-ui';

export const columns = [
  {
    title: '交易时间',
    key: 'createdTime',
    width: 80,
  },
  {
    title: '交易金额',
    key: 'txAmt',
    width: 80,
  },
  {
    title: '币种',
    key: 'ccy',
    width: 80,
  },
  {
    title: '实付金额',
    key: 'payAmt',
    width: 80,
  },
  {
    title: '币种',
    key: 'ccy',
    width: 80,
  },
  {
    title: '会员号',
    key: 'memNo',
    width: 80,
  },
  {
    title: '交易流水号', 
    key: 'txNo',
    width: 80,
  },
  {
    title: '商品订单号',
    key: 'prodOrdNo',
    width: 80,
  },
  {
    title: '有无退款',
    key: 'refundStatus',
    width: 80,
  },
  {
    title: '折扣编码',
    key: 'disctNo',
    width: 80,
  },
  {
    title: '折扣金额',
    key: 'disctAmt',
    width: 80,
  },
  {
    title: '币种',
    key: 'ccy',
    width: 80,
  },
  {
    title: '状态',
    key: 'status',
    width: 80,
  },
  {
    title: '交易失败原因',
    key: 'remarks',
    width: 80,
  },
];
