
import bsinRequest from "@/utils/bsinrequest";

// 删除角色信息
export async function deleteRoleInfo(params: object) {
    console.log('viewRoleInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RoleSVC",
                stdIntfcInd: "delete",
            },
            body: {
                ...params,
            }
        }
    })
}

