
import bsinRequest from "@/utils/bsinrequest";

// 获取用户信息
export async function viewUserInfo(params: object) {
    console.log('addUserInfo');

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "UserSVC",
                stdIntfcInd: "view",
            },
            body: {
                ...params,
            }
        }
    })
}

