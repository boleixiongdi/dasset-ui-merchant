
import bsinRequest from "@/utils/bsinrequest";

// 商户发起退款
export async function initiateRefund(params: object) {

    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "RefundSVC",
                stdIntfcInd: "refund",
            },
            body: {
                ...params,
            }
        }
    })
}

