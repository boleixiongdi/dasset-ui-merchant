
import bsinRequest from "@/utils/bsinrequest";

// 修改交易密码
export async function updateTragePassword(params: object) {
    console.log('updateTragePassword');
    return bsinRequest('/agrs', {
        method: 'POST',
        data: {
            sysHead: {
                stdSvcInd: "TxPwdMngSVC",
                stdIntfcInd: "update",
            },
            body: {
                ...params,
            }
        }
    })
}
