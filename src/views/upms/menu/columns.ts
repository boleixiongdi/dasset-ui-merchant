
export const columns = [
  {
    title: '菜单ID',
    key: 'menuId',
    width: 120,
  },
  {
    title: '菜单名称',
    key: 'name',
    width: 120,
  },
  {
    title: '菜单英文名称',
    key: 'nameEn',
    width: 120,
  },
  {
    title: '标准服务标识',
    key: 'stdSvcInd',
    width: 120,
  },
  {
    title: '标准接口标识',
    key: 'stdIntfInd',
    width: 120,
  },
  {
    title: '父菜单名称',
    key: 'parentName',
    width: 80,
  },
  {
    title: '优先排序',
    key: 'priority',
    width: 80,
  },
];
